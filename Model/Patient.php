<?php
    include_once("./Person.php");

    class Patient extends Person{
        /* Therapies that the patient is currently undergoing and already underwent. */
        private $therapies;
        /* Diseases that the patient is currently suffering from and already underwent.
         * Symptoms appear in a separate category. */
        private $diseases;
        private $symptoms;
        /* Hospitalization room. May change according to disease development. */
        private $room;
        /* Bed id: let's just suppose there is no difference between wards. */
        private $bedID;
        private $isUnderExam;
        
        public function __constructs($name, $surname, $diseases, $symptomps, $room, $bedID){
            parent::__constructs($name, $surname);
            
            $this->bedID=$bedID;
            $this->therapies=null;
            $this->room=$room;
            $this->isUnderExam=false;
            
            /* Set up $diseases */
            if($diseases==null)
                $this->$diseases=null;
            else{
                /* Add known diseases */
                $this->diseases=array();
                foreach($diseases as $addingDisease)
                    $this->diseases[]=$addingDisease;
            }
            
            /* Setup symptoms */
            if($symptomps==null)
                $this->symptomps=null;
            else{
                /* Add known diseases */
                $this->symptoms=array();
                foreach ($symptomps as $symptom)
                    $this->symptoms[]=$symptom;
            }
        }
        
        public function getTherapies(){
            return $this->therapies;
        }
        
        public function getDiseases(){
            return $this->diseases;
        }
        
        public function getSymptoms(){
            return $this->symptoms;
        }
        
        public function getBedID(){
            return $this->bedID;
        }
        
        private function hasThisDisease($disease){
            foreach($this->diseases as $patient_disease)
                if()
                   
                
            return false;            
        }
        
        public function addDisease($newDisease){
            if(get_class($newDisease)=="Disease"){
                if(!hasThisDisease($newDisease)){
                    $this->diseases[]=$newDisease;
                    return true;
                }
                hasThisDisease($newDisease).editDisease($newDisease);
                return true;
            }
            return false;
        }
        
    }
?>
