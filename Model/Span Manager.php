<?php
   include_once("./Span.php");
   
   class SpanManager{
      private $spans;
      private $firstSpan;
      private $lastSpan;
      
      // Add a new Span. returns: boolean (success/insuccess)
      public function addSpan($newSpan){
         
         /* In che ordine è meglio tenere gli eventi? 
         
            I. Cronologico
            
               Pro:
                  -Facilità di ricerca.
                  -Velocità di ricerca.
                  -Possibilità di delimitare archi temporali e di creare
                   una cronologia
               Contro:
                  -Ricerca di punto di inserimento per ogni aggiunta.
                   (Ordinamento all'accesso escluso)
                   
                   
            II. ???
         */
         
         // Find insert place
         foreach($spans as $currentSpan)
            if($currentSpan->isOverlapping($newSpan))
               return false;
         
         // $newSpan accepted
         // First Span added
         if($spans==null){
            $spans=array($newSpan);
            $firstSpan=$spans;
            $lastSpan=$spans;
            return true;
         }
         
         
         
      }
      
      // Search for a Span by $title. returns: Span Object/null (found/not found)
      public function searchByTitle($name){
         if($name==null || !is_string($name))
            throw new InvalidArgumentException();
         
         foreach($spans as $currentSpan)
            if($currentSpan->getTitle()==$name)
               return $currentSpan;
         
         return null;
      }
   
      // Search for a Span by date. returns: Span Object | Exception in the opposite case.
      public function searchByDate($date){
         if(!isADate($date))
            throw new notADateException();
         
         if($spans!=null)
            foreach($spans as $currentSpan)
               if($currentSpan->isOverlapping($currentSpan))
                  return $currentSpan;
         
         return null;
      }
      
      // Move a span, provided it does not cause conflict. returns boolean (success/unsuccess)
      public function moveSpan($editSpan, $newStart, $newEnd){
         if($spans==null)
            throw new emptySpansException();
            
         if(!isADate($newStart) || !isADateInterval($newEnd))
            throw new notADateException();
         
         if(!isASpan($editSpan))
            throw new notASpanException();
         
         foreach($spans as $currentSpan)
            if($currentSpan->isOverlapping($newStart) || $currentSpan->isOverlapping($newEnd))
               throw new spanOverlappingException();
               
         $editSpan->setStart($newStart);
         $editSpan->setEnd($newEnd);
         return true;
      }
      
   }
?>