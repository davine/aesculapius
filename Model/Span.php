<?php
	
	// Returns whehter $date is a date. returns: boolean 
	function isADate($date){
	   if(get_class($date)=="DateTime")
		  return true;
	  return false;
	}
	
	// Returns whehter $interval is a DateInterval Object. returns: boolean
		function isADateInterval($interval){
		   if(get_class($interval)=="DateInterval")
		      return true;
	      return false;
		}
		
  class Span{
		private $startsOn;
		private $span;
    
		// Construct a Span Object that starts on $startsOn and lasts $span. returns: boolean (success/unsuccess)
		public function __construct(DateTime $startsOn, DateInterval $span){
		   //if(!isADate($startsOn)  || !isADateInterval($span))
		   //    throw new Exception();
		      
			// Tip: $span==null means an occurring disease.
			if((get_class($span)=="DateInterval" || $span==null)){
	         $this->startsOn=$startsOn;
				$this->span=$span;
				return true;
			}
			
			return false;
		}
		
		// Returns the start of the event. returns: DateTime Object
		public function getStart(){
		   return $this->startsOn;
		}
		
		// Returns the end of the event. returns: DateTime Object
		public function getEnd(){
		   $end=clone $this->startsOn;
		   $end->add($this->span);
		   
		   return $end;
		}
		
		// Returns whether $date1 occurs later than $date2. returns: boolean
		public static function isInTheFuture($date1, $date2){
		   if(!isADate($date1))
		      throw (new notADateException());
		      
	      // $date2==null to check with the current time
	      if($date2==null)
	         $date2=new DateTime(null, new DateTimeZone('Europe/Rome'));
	      
	      if($date1->diff($date2, false)->format('%R')=="-")
	         return true;
	         
         return false;
		}
		
		// Returns whether a DateTime is later than anotherone. returns: boolean
		public function isLaterThan($otherSpan){
			if(!isADate($otherSpan) || $otherSpan==null)
			   throw new notADateException();
		   
		   if(isInTheFuture($this->startsOn,$otherSpan->getEnd()))
		      return true;
	      
	      return false;
		}
		
		// Returns whether this Span is overlapping with another. returns: boolean
		public function areOverlapping($otherSpan){
         if(isADate($otherSpan))
            if(($otherSpan->getStart()->isLaterThan($this->startsOn)) && !($otherSpan->getStart()->isLaterThan($this->getEnd())))
               return true;
         else
            throw new notADateException();
         
         return false;
		}
		
		
	}
	
	$a=new DateTime(null,new DateTimeZone('Europe/Rome'));
	$b=new DateInterval('P3M2D');
	try{		
		$c=new Span($a,$a);
	} catch(Exception $e){
		echo "A mistake";
	}
		
?>
